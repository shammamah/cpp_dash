# Example C++/Dash app

## About

This application can take files uploaded via the Dash app web
interface, perform a computation on them, and return the result in a
heatmap.

## How to run this app

First, ensure that `pip` and `virtualenv` are both installed. This app
runs on Python 3.

### Install all Python dependencies

```
$ pip install -r requirements.txt
```

### Run the app

```
$ python3 app.py
```

### Visit the app

Go to: [localhost:8050](localhost:8050).