import os
import tempfile
import base64

import pandas as pd

import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html

import plotly.graph_objs as go

app = dash.Dash(__name__)

os.system('g++ matrix.cpp')

app.layout = html.Div([
    dcc.Upload(
        id='upload-graph-data',
        children='Upload data here.'
    ),

    dcc.Dropdown(
        id='test-drop',
        options=[
            {'label': i, 'value': i}
            for i in range(5)
        ]
    ),

    dcc.Loading(dcc.Graph(
        id='heatmap-graph'
    ))
])


@app.callback(
    Output('heatmap-graph', 'figure'),
    [Input('upload-graph-data', 'contents')]
)
def testing(contents):

    if contents is None:
        return dict(data=[])

    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)

    with tempfile.NamedTemporaryFile(delete=False) as f:
        f.write(decoded)
        f.flush()

        # run the cpp file
        os.system('./a.out {}'.format(f.name))

    # read the output and display in a heatmap
    data = pd.read_csv('matrix.csv', header=None)

    return go.Figure(data=go.Heatmap(
        z=data.values,

    ))


app.run_server(debug=True)
