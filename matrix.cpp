using namespace std;

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<time.h>



/*
 * An example function showing how to access and preprocess the file
 * contents that have been uploaded from within the Dash app.
 * You can change the return type to whatever format is easiest to
 * perform the calculation on; I just return an int for simplicity.
 */
int readGraphFile(char *filename) {
  
  ifstream graphFile;
  graphFile.open(filename);

  // now you can do whatever you want with the file; I've chosen to
  // just return the number of lines in it

  int x = 0;
  string line;
  
  cout << "Reading file...\n";
  
  while(getline(graphFile, line)){
    x++;
  }
  
  graphFile.close();

  // file preprocessing finished; return the result

  cout << "Done!\n\n";
  
  return x;
  
}

/*
 * An example function showing a sample computation based on the
 * contents of some file.
 */
vector<vector<double> > computeMatrix(char *filename) {


  vector<vector<double> > matrix;

  // computing the actual matrix; here I have used the contents of the
  // preprocessed file to determine a seed for a RNG that is used to
  // populate a 5x5 matrix

  int example_file_read = readGraphFile(filename);

  cout << "Computing matrix...\n";

  srand(example_file_read);
  
  for(int i=0; i<5; i++) {
    vector<double> row;
    for(int j=0; j<5; j++) {
      row.push_back((double)(rand() % 100) / 100.0);
    }
    matrix.push_back(row);
  }

  // simulate long computation time for Dash app simulation
  usleep(2000000);

  // computation finished; return the result

  cout << "Done!\n\n";
  
  return matrix;

}

/*
 * The cpp program will receive as a command-line argument the name of
 * a temp file that holds the contents of the file that has been
 * uploaded.
 */ 
int main(int argc, char *argv[])
{

  vector<vector<double> > matrix = computeMatrix(argv[1]);

  // write to csv for easy parsing by the pandas library in python
  
  ofstream matrixfile;
  matrixfile.open("matrix.csv");

  for(int i=0; i<(int)matrix.size(); i++) {
    for(int j=0; j<(int)matrix.at(i).size(); j++) {
      matrixfile << matrix.at(i).at(j);
      if(j<(int)matrix.at(i).size() - 1) { matrixfile << ","; }
    }
    matrixfile << "\n";
  }

  matrixfile.close();

}

